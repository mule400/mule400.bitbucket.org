= IBM i (AS400) Connector Release Notes
:keywords: AS400, AS/400, IBM i, iSeries, Data Queue, Program Call, API
:toc: right
:source-highlighter: prettify

Infoview Systems developed IBM i (AS400) connector for Mulesoft Anypoint and Web
Transaction Framework to simplify consuming and producing APIs on IBM i
(AS/400, iSeries, System i). The latest release enables direct IBM i
program call and Service program procedure call. This feature greatly accelerates the delivery and reduces the cost of
developing APIs that wrap existing back-end business logic programs. 

== Version 2.1.2 - August 2020
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features

* Added socket timeout, number of retries, and  max time to live to connection configuration
* Enabled standard reconnection strategy and notifications in case of connection failed events
* Added explicit Data Queue entry and key length to support writing into remote (DDM) keyed Data Queue 


== Version 2.1.1 - May 2019
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features

* Added debug logging of the host server job executing program or command call  

=== Fixed in this release

* Program Call Reconnection fix - depending on OS version and PTF level, some customers may experience issues with socket timeout. Implemented "hard" connection reset in case of the program call socket timeout or connection interruptions. 

== Version 2.1.0 - October 2018
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features

* Added "procedure name" field to Program Call operation to support calling Service Program Procedurs directly from Mule application
* Added External Structure support to Data Queue Listen, Read and Write operations, to further simplify exchanging the data between Mule application and IBM i back-end programs via data queues. 

== Version 2.0.0 - June 2018

=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
Mule 4 and new SDK introduced fundamental changes to the core runtime and the extension toolset. As a result, there are several changes in the Mule 4 AS400 connector configuration and operations.
Below is a list of key items that must be addressed when porting the applications from Mule 3.x to Mule 4.XML
* Configuration - connection properties are defined in a separate nested element as400:config-connection
* Command Call - the command is passed in the nested tag as400:cmd
* Write Data Queue - the message entry is passed in the nested tag as400:dq-entry
* Read Data Queue - the entry key is placed into attributes.key 
* Program Call - the parameter value attribute was changed from parmValue-ref to parmValue; for data structure element definitions, the tag as400:data-structure-element is renamed to as400:parameter. 

=== Features

This release ports all previously implemented connector features to be compatible with Mule 4

=== Fixed in this Release
None

=== Known Issues
None


== Version 1.0.2 - October 2017

=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 3.6.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features

* Program Call operation / processor that has no limit on the number of parameters,
supports most RPG data types (including date / time / timestamp), as well as data structures and arrays
* Secure TLS communications with IBM i (AS400) is now supported

=== Fixed in this Release
* Blank library list in Connection definition caused connection issues for IBM i OS versions 7.2 and 7.3

=== Known Issues
* Due to Anypoint Studio behavior the parameter definitions added via connector GUI may be out of
sequence after the Mule application changes are saved. Developers must go to XML view of the Mule code and
confirm / update the parameter sequence to match IBM i program parameters.

== Version 1.0.0 - November 2015

=== Compatibility Matrix

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 3.5.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Supported Operations:
[cols=",",options="header"]
|===
|Operation |Description
|Read Data Queue (Message Source) | Perpetually listen for new messages arriving to specific data queue
|Read Data Queue (Processor) | Read messages from specific data queue as part of Mule flow
|Write to Data Queue | Write messages to data queue
|Command Call | Execute AS400 command call
|===


=== Features and Functionality

* Works with keyed and non-keyed data queues
* Enables "peek" functionality to read message without removing it from the queue
* Automatically recovers stale connections

=== Fixed in this Release
None.

=== Known Issues
None.


== See Also

* Learn how to http://www.mulesoft.org/documentation/display/current/Anypoint+Exchange#AnypointExchange-InstallingaConnectorfromAnypointExchange[Install Anypoint Connectors] using Anypoint Exchange.
* Access MuleSoft's http://forum.mulesoft.org/mulesoft[Forum] to pose questions and get help from Mule's broad community of users.
* To access MuleSoft's expert support team, http://www.mulesoft.com/mule-esb-subscription[subscribe] to Mule ESB Enterprise and log into MuleSoft's http://www.mulesoft.com/support-login[Customer Portal].
