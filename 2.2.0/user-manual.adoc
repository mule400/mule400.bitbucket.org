= IBM AS/400 Connector
:keywords: AS400, AS/400, IBM i, iSeries, Data Queue, Mulesoft, Salesforce, Infoview Systems
:imagesdir: .\images
:source-highlighter: prettify
:toc: right


[green]#Certified#


== Introduction

Infoview Systems AS400Gateway suite of products eliminates the stress and impact of IBM i / AS400 legacy system integration on development teams, minimizes the time and resources put into building integrations by hand, and enables non-IBM i developers to unlock legacy business logic and data directly from the comfort of their modern integration development stack. Certified and rigorously tested by MuleSoft, the connector was designed to accelerate IBM i / AS400 integrations with other systems and services.  

We are a global cross-platform service team with a unique fluency in legacy and modern technology stacks, including Mulesoft and IBM i / AS400. Infoview’s dedicated customer success representative coordinates just-in-time technical assistance and support to client teams ensuring you have all the help you may need, when you need it. We are more than happy to provide a trial license for our products, participate in discovery sessions, run live demos for typical integration scenarios with our Gateway products, as well as assist with or perform a proof of concept based on particular use cases.

link:http://www.infoviewsystems.com/contact-us[Contact us] for connector pricing info, trial license, or support questions. 

== IBM i / AS400 Connector for Mulesoft Overview

The IBM AS/400 was first introduced in 1988 and evolved into a very stable modern all-purpose integrated solution 
that requires little or no management. The system is able to run core line of business applications securely and predictably, focusing on quality of service and availability and offering a  
compelling total cost of ownership for integrated on-premises solutions. IBM made several changes to the server and OS name (iSeries, System i, IBM i, Power Systems for i) but most still 
refer to it as AS/400.

The IBM i platform offers a number of integration options including PHP, Java, WebSphere, specialized lightweight web service containers, FTP, SMTP / emails, DB2 interfaces, data queues,
integrated file systems - IFS, as well as number of products offered by IBM and Third party vendors. The main benefit of using "native" options such as Program Calls and Data Queues is that 
IBM i development team does not have to learn another language or purchase and support another technology in order to build integration layer, and can easily communicate with
external systems using only traditional development tools.        

Program Call is the most straightforward and low code option for exposing IBM i business logic as a reusable asset. The IBM i connector enables direct program calls from Mule application, passing parameters into the program and receiving the results back in real time. 

Data queues are native IBM i objects designed primarily for inter-process communications. They are lightweight persistent queues that support processing by a key, FIFO or LIFO. The majority of
integration use cases can be implemented with the pair of request and response Data Queues. Source system places a message to request data queue and waits for acknowledgement message on 
response data queue. The target system receives and processes a message from the request data queue then places the acknowledgement to the response data queue.   

The IBM i connector makes it easy to build Mule code that talks to IBM i applications. Infoview Systems also built the IBM i ‘Web Transaction Framework’ that makes it very fast
and easy to develop IBM i code that communicates with Mule.

== Prerequisites

The connector is designed to work with IBM i objects (programs, service programs, data queues and commands) and therefore this document assumes you are familiar with IBM i operational and development environments and tools.

This document assumes you are familiar with Mule, Anypoint Connectors,
and Anypoint Studio. 

This document describes implementation examples within the context of Anypoint Studio, Mule ESB’s graphical user interface, and, in parallel,
includes configuration details for doing the same in the XML Editor.

== Dependencies

* Access to IBM i server
* Mule Enterprise runtime or Anypoint Studio

== Compatibility Matrix

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1 or higher
|Anypoint Studio	| 7.1 or higher
|IBM i / OS400  | V5R4 or higher
|=======

== Installing and Configuring

=== Installation instructions
To install the connector open the `pom.xml` file in the project root folder, and add the following dependency:

.as400-connector
[source,xml]
----
<dependency>
    <groupId>com.infoview.mule</groupId>
    <artifactId>as400-connector</artifactId>
    <version>2.1.2</version>
    <classifier>mule-plugin</classifier>
</dependency>
----

After that you should be able to find AS400 connector in the Mule Pallete window.

image::connector_in_mule_pallete.png[connector_on_pallete]

The IBM i connector requires a license key from Infoview to enable access to specific IBM i system(s). The connector license must be installed into $MULE_HOME/conf folder for on-prem runtimes, or
included into application archive that will be deployed to CloudHub. Please contact Infoview Systems IBM i Connector support team at *(734) 293-2160* or via email mule400@infoviewsystems.com

In addition to Infoview license, the connector can only run on Mulesoft Enterprise runtimes.
  
=== IBM i server configuration and requirements
* IBM i must have ports 446, 449, 8470, 8472,8473,8475 and 8476 accessible from Mule runtime for non-SSL communications, and ports 448, 449, 9470, 9472, 9473, 9475 and 9476 accessible for SSL communications
* IBM i must have **CENTRAL, *DTAQ, *RMTCMD, *SIGNON and *SRVMAP* host servers running in the QSYSWRK subsystem
* If secure TLS connection is used, the TLS certificate must be applied to Central, Data Queue, Remote Command, File, Signon, and DDM / DRDA services in Digital Certificate Manager
* IBM i user ID must be authorized to perform the operations on the intended IBM i objects
* If there's an additional security software that locks down the remote execution functionality, the IBM i user ID defined for connector configuration must be allowed to execute remote calls and access database, IFS and DDM services

== Configuring the IBM i Connector

To use the IBM i connector in your Mule application, you must configure global IBM i Connection Strategy element that can be used by all the IBM i connectors in the application

Follow these steps to configure IBM i connector in a Mule application:

. Click the *Global Elements* tab at the base of the canvas, then click *Create*.
. In the *Choose Global Type* menu, use the filter to locate and select *AS400 Config*, then click *OK*.
. Configure the parameters according to the table below.
+
image::global_configuration.jpg[global_config]
+
[width="100%",cols="35,45,20,20",options="header"]
|===
|Parameter |Description |Mandatory |Default Value
|*Name* |Enter a name for the configuration so it can be referenced later by the `config-ref` parameter in the flow. |Optional |AS400_Config
|*URL* |Enter the IBM i endpoint. |Required |--
|*UserID* |Enter the username that can connect to IBM i. |Required |--
|*Password* |Enter the password for the above IBM i. |Required |--
|*IASP* |Enter the IASP name. |Optional |--
|*LibraryList* |Enter a comma separated list of libraries (if any) that needs to be added to the library list for Command call execution. |Optional |--
|*secureConnection* |When _True_, the connector will try to establish secure SSL / TLS connection with IBM i endpoint. |Optional |False
|*Socket Timeout* | Max socket timeout value for the AS400 connection. The value of -1 indicates the default system value is used |Optional | -1
|*Connection Retries* | Number of connection retries internally within AS400 connector configuration before the exception is raised to Mulesoft runtime and connection management. |Optional |3
|*Connection TTL* | Connection Active Time (Time to Live), 0 - no limit. |Optional |0
| *<<TLS>>* | Reference to a TLS config element. This will enable secure connection for this config. |Optional |--
|===
. Click *OK* to save the global connector configurations.
. Return to the Message Flow tab in Studio.

[[TLS]]
=== TLS Configuration

[width="100%",cols="35,45,20,20",options="header"]
|===
|Parameter |Description |Mandatory |Default Value
|*<<Truststore>>* |Truststore is used to store certificates from Certified Authorities (CA) that verify the certificate presented by the server in SSL connection. | |
|===

[[Truststore]]
=== Truststore
[width="100%",cols="35,45,20,20",options="header"]
|===
|Parameter |Description |Mandatory |Default Value
|*Path* |The location (which will be resolved relative to the current classpath and file system, if possible) of the trust store. |Required |--
|*Password* |The password used to protect the trust store. |Required |--
|*Type* |The type of store used. |Optional |--
|*Algorithm* |The algorithm used by the trust store. |Optional |--
|*Insecure* |If true, no certificate validations are performed, rendering connections vulnerable to attacks. Use at your own risk. |Optional |--
|===

== Connector Operations
The IBM i connector is an operation-based connector, which means that when you add the connector to your flow, you need to configure a specific operation the connector is intended to perform. The connector supports the following operations:

[cols=",",options="header"]
|===
|Operation |Description
|Read Data Queue (Message Source) | Perpetually listen for new messages arriving to specific data queue.
|Read Data Queue (Processor) | Read messages from specific data queue as part of Mule flow.
|Write to Data Queue | Write messages to data queue.
|Command Call | Execute IBM i command call.
|Program Call | Execute IBM i program or Service program procedure.
|===

=== Configuring the IBM i Data Queue Listener (Message Source)

. Drag the *Read data queue component* onto the Message Source area of the flow and select it to open the Properties Editor console.
. Configure these connector parameters:
+
image::source_config.png[source_config]
+
[width="100%",cols="35,45,20,20",options="header",]
|===
|Field |Description |Mandatory |Default
|*Display Name* |Enter a unique label for the connector in your application. |Optional |Read data queue
|*Connector Configuration* |Select the global IBM i connector element that you just created. |Required |--
|*Data Queue* |Enter Data Queue name. |Required |--
|*Library* |Enter Data Queue library name. |Required |--
|*Key* |Must be specified for keyed data queues and blank for non-keyed data queues. For reading any message from data queue, enter ' '. |Optional |--
|*Key Search Type* |Must be specified for keyed data queues. For reading any message from data queue, enter Greater or Equal. |Optional |Empty
|*Keep messages in Queue* |Ensure it is unchecked. |Required |false
|*Format File Name* |Optional parameter allows treating data queue entry as an externally defined data structure. When defined, the connector will dynamically retrieve the record format from the the specified IBMi file, and parse the received data queue entry into the map of field name / value pairs. The connector will perform the type conversion, supporting all types such as packed, date / time etc |Optional |--
|*Format File Library* |When format file is specified, the format file library can also be specigied, otherwise the format file will be located based on the connection library list. |Optional |*LIBL
|*Polling period* |Time between message reads, ms. Can be used as crude throttling configuration |Optional | 1
|*Number of consumers* |Number of Data Queue Listener threads. |Optional |4
|===

If message is received from the data queue, message text will be placed into payload and message key (if any) will be saved into *#[attributes.key]*

The output payload format depends on whether the external data format. When the format file has been specified, the output is a Map, where keys are format field names, and the values represent the data for each field. When no format file has been specified, the data queue entry will be returned as a String. 

=== Configuring the IBM i Data Queue Reader (Message Processor)

. Drag the *Read data queue processor* component onto the Message Flow area (other than message source) and select it to open the Properties Editor console.
. Configure these connector parameters:
+
image::read_processor_config.png[read_config]
+
[width="100%",cols="35,45,20,20",options="header",]
|===
|Field |Description |Mandatory |Default
|*Display Name* |Enter a unique label for the connector in your application. |Required |Read data queue processor
|*Connector Configuration* |Select the global IBM i connector element that you just created. |Required |--
|*Data Queue* |Enter Data Queue name. |Required |--
|*Library* |Enter Data Queue library name. |Required |--
|*Key* |Must be specified for keyed data queues and blank for non-keyed data queues. For reading any message from data queue, enter `' '`. |Required |--
|*Key Search Type* |Must be specified for keyed data queues. For reading any message from data queue, enter Greater or Equal. |Required |Empty
|*Keep messages in Queue* |Ensure it is unchecked unless the intent is to leave the message in the queue after reading. |Required |False
|*Format File Name* |Optional parameter allows treating data queue entry as an externally defined data structure. When defined, the connector will dynamically retrieve the record format from the the specified IBMi file, and parse the received data queue entry into the map of field name / value pairs. The connector will perform the type conversion, supporting all types such as packed, date / time etc |Optional |--
|*Format File Library* |When format file is specified, the format file library can also be specigied, otherwise the format file will be located based on the connection library list. |Optional |*LIBL
|===

If message is received from the data queue, message text will be placed into payload and message key (if any) will be saved into *#[attributes.key]*

The output payload format depends on whether the external data format. When the format file has been specified, the output is a Map, where keys are format field names, and the values represent the data for each field. When no format file has been specified, the data queue entry will be returned as a String. 


=== Configuring the IBM i Data Queue Writer

. Drag the *Write data queue* component onto the Message Flow area (other than message source) and select it to open the Properties Editor console.
. Configure these connector parameters:
+
image::write_processor_config.png[write_config]
+
[width="100%",cols="35,45,20,20",options="header",]
|===
|Field |Description |Mandatory |Default
|*Display Name* |Enter a unique label for the connector in your application. |Optional |Write data queue
|*Connector Configuration* |Select the global IBM i connector element that you just created. |Required |--
|*Data Queue* |Enter Data Queue name. |Required |--
|*Library* |Enter Data Queue library name. |Required |--
|*Data Entry* |Enter Data Queue Message Text. |Optional |--
|*Key* |Must be specified for keyed data queues and blank for non-keyed data queues. For reading any message from data queue, enter `' '`. |Optional |--
|*Format File Name* |Optional parameter allows treating data queue entry as an externally defined data structure. When defined, the connector expects the JSON string to be passed as Data Queue Entry. The connector will dynamically retrieve the record format from the the specified IBMi file, parse the JSON string passed into the processor, into a binary byte array that can be interpreted on IBM i side as a data structure. The JSON string must have a list of elements with element name matching the format file column name, and associated value. The connector will perform the type conversion, supporting all types such as packed, date / time etc |Optional |--
|*Format File Library* |When format file is specified, the format file library can also be specigied, otherwise the format file will be located based on the connection library list. |Optional |*LIBL
|*DQ Entry Length* |Max DQ Entry Length. When specified and greater than 0, the parameter value will be truncated to fit the max length. |Optional |0
|*DQ Key Length* |Max DQ Key Length. When specified and greater than 0, the parameter value will be used (instead of dynamically retrieving it from DQ definitions on the server). |Optional |0
|===

=== Configuring the IBM i Command Call

. Drag the *Command call* component onto the Message Flow area (other than message source) and select it to open the Properties Editor console.
. Configure these connector parameters:
+
image::command_call_processor_config.png[command_call_config]
+
[width="100%",cols="35,45,20,20",options="header",]
|===
|Field |Description |Mandatory |Default
|*Display Name* |Enter a unique label for the connector in your application. |Optional |Command call
|*Connector Configuration* |Select the global IBM i connector element that you just created. |Required |--
|*Command* |Enter IBM i command. |Optional |`#[payload]`
|===

=== Configuring the IBM i Program Call and Service Program Procedure Call

. Drag the *Program call* component onto the Message Flow area (other than message source) and select it to open the Properties Editor console.
. Configure these connector parameters:
+
image::program_call_processor_config.png[program_call_config]
+
[width="100%",cols="35,45,20,20",options="header",]
|===
|Field |Description |Mandatory |Default
|*Display name* |Enter a unique label for the connector in your application. |Optional |Program call processor
|*Connector Configuration* |Select the global IBM i connector element that you just created. |Required |--
|*Program or Service Program name* | Enter the program or Service Program name. | Required |--
|*Program library* | Enter the library name.| Required |--
|*Service Program Procedure Name* | Enter the name of service program procedure.| Optional |--
|*Procedure Returns Value* |Indicator if the service program procedure returns a value.  | Optional | False
|*Thread safe* |Indicator if the program is thread safe. | Optional | False
|*Parameters* |List of definitions and value references of program parameters. | Optional |None
|===
+
There are several limitations for Service Program procedure call, imposed by the IBM i (AS400) external Service Program call API (QZRUCLSP):

    * Max 7 parameters are allowed (each parameter can be data structure or array of data structures)
    * If the procedure returns a value, it must be an integer

. In the case when it's need to run program with arguments, the *Parameters* property must be set to `Expression` or `Edit inline`, and list of definitions and value references of program parameters must be specified.
+
image::program_call_parameter_config.png[]
+
[width="100%",cols="35,45,20,20",options="header",]
|===
|Field |Description |Mandatory |Default
|*Parameter name* |Enter a unique label for the program parameter |Required |--
|*Data type* |Select the IBM i data type. |Required |Empty
|*Length* |Parameter length, bytes |Optional |0
|*Decimal positions* |Decimal points  | Optional |0
|*Usage* |In / Out / Inout |Required |Empty
|*Count* |For array parameters, number of elements in the array |Optional |1
|*Data structure element* |Nested parameter definitions for each data structure element |Optional |None
|*Parameter value* |Enter the parameter value. |Optional |--
|===
 

== Contact Us

link:http://www.infoviewsystems.com/contact-us[Contact us] for connector pricing info, trial license, or support questions. 

== See Also

* More IBM i technical articles, source code samples and how-to's refer to  link:http://www.infoviewsystems.com/blog[Infoview Blog]
* link:https://blogs.mulesoft.com/dev/connectivity-dev/how-to-build-an-ibm-i-as400-api-in-15-minutes/[Build IBM i API in 15 minutes]
*  link:https://videos.mulesoft.com/watch/SXNPhnXutmy6tS4vqYUo9Y[Mulesoft Legacy Modernization webinar featuring IBM i connector]
