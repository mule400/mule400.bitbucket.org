= IBM i (AS400) Connector Release Notes
:keywords: AS400, AS/400, IBM i, iSeries, Data Queue, Program Call, API, Mulesoft, AS400 Gateway, Salesforce, Infoview Systems
:toc: right
:source-highlighter: prettify

Infoview Systems AS400Gateway suite of products eliminates the stress and impact of IBM i / AS400 legacy system integration on development teams, minimizes the time and resources put into building integrations by hand, and enables non-IBM i developers to unlock legacy business logic and data directly from the comfort of their modern integration development stack. Certified and rigorously tested by MuleSoft, the connector was designed to accelerate IBM i / AS400 integrations with other systems and services.  

We are a global cross-platform service team with a unique fluency in legacy and modern technology stacks, including Mulesoft and IBM i / AS400. Infoview’s dedicated customer success representative coordinates just-in-time technical assistance and support to client teams ensuring you have all the help you may need, when you need it. We are more than happy to provide a trial license for our products, participate in discovery sessions, run live demos for typical integration scenarios with our Gateway products, as well as assist with or perform a proof of concept based on particular use cases.

link:http://www.infoviewsystems.com/contact-us[Contact us] for connector pricing info, trial license, or support questions.

== Version 2.6.0 - May 2023
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features and fixes

* Feature: Added the ability to refresh format file if It has been updated

* Feature: Added support for action on errors those occur on failed mapping with format file(Format Exception Action) at Read Data Queue and Data Queue Listener level: DISCARD, PLACE_BACK_INTO_QUEUE

* Feature: Added support for CCSID and DBCS parameters for Program Call operation

* Feature: Added support for dynamic format file definition for Read Data Queue operation by setting starting position for format file name(Format File Name Position) and format file library(Format File Library Position) in Data Queue message

* Feature: Added the ability to reset Library List on each Program Call execution

* Fix: Improved performance for Program Call operation if it executed with Library List

* Fix: Fixed Command Call output to retain the input payload

== Version 2.5.0 - August 2022
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features and fixes

* Feature: Added support for new Infoview connector license management tool, while maintaining backward compatibility with existing license formats.
* Feature: Added support for Library List for Program Call operation
* Feature: Added support for mode of adding user libraries to library list(Library List Mode) at configuration and program call level: ADD_FIRST, ADD_LAST, REPLACE
* Feature: Added support of job metadata in attributes.as400JobMetadata

== Version 2.4.0 - February 2022
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features and fixes

* Feature: Nested data structures for program call operations.
* Fix: Issue with reading files from S3 and FTP.


== Version 2.3.1 - November 2021
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features and fixes

* Fix: In some cases, the Library list is not properly initialized when auto-recoverining the connection for Program Call or Command Call operations
* Fix: Do not end and restart the connection after Program Call fails due to time out
* Fix: When running in Debug mode on Mule runtime 4.4.0, the Mule log shows error message when logging the input parameter values
* Fix: Implement program call auto-retry in case of any I/O exceptions during the call (previously the program call auto-retry was specifically implemented for Connection Dropped exception only)
* Fix: Changed the Data Queue Listener default connection retry interval from 60 seconds to 1 second to ensure near real time recovery for interrupted connections 


== Version 2.3.0 - October 2021
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features and fixes

* Ability to reference external license instead of packagin with Mulesoft application, supporting FILE, HTTP / HTTPS, AWS S3, FTP, SMB, CLASSPATH transports
* Added Reconnection period and retries specifically for DQ Listener to better control the reconnection behavior
* Added Call timeout to Program call operation
* Fix: secure connection could not be established without defining tls configuration
* Fix: removed reconnection attempts for Data Queue Listener when the data queue or library is not found

== Version 2.2.0 - February 2021
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features and fixes

* Added Transport Layer Security (TLS) support to provide secure connection between Mule and IBM i / AS400
* Jt400 library updated to v10.3. This library release includes a fix for the issue where program call method does not 
accept more than 35 parameters for OS 7.3 and higher. JT400 upgrade now allows up to 255 parameters to be passed as part of the remote program call.

== Version 2.1.3 - October 2020
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features and fixes

* Data Queue Source shutdown timeout was adjusted from 30 min to 5 sec to ensure a correct connection closure

== Version 2.1.2 - August 2020
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features

* Added socket timeout, number of retries, and  max time to live to connection configuration
* Enabled standard reconnection strategy and notifications in case of connection failed events
* Added explicit Data Queue entry and key length to support writing into remote (DDM) keyed Data Queue 


== Version 2.1.1 - May 2019
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features

* Added debug logging of the host server job executing program or command call  

=== Fixed in this release

* Program Call Reconnection fix - depending on OS version and PTF level, some customers may experience issues with socket timeout. Implemented "hard" connection reset in case of the program call socket timeout or connection interruptions. 

== Version 2.1.0 - October 2018
=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features

* Added "procedure name" field to Program Call operation to support calling Service Program Procedurs directly from Mule application
* Added External Structure support to Data Queue Listen, Read and Write operations, to further simplify exchanging the data between Mule application and IBM i back-end programs via data queues. 

== Version 2.0.0 - June 2018

=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 4.1.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
Mule 4 and new SDK introduced fundamental changes to the core runtime and the extension toolset. As a result, there are several changes in the Mule 4 AS400 connector configuration and operations.
Below is a list of key items that must be addressed when porting the applications from Mule 3.x to Mule 4.XML
* Configuration - connection properties are defined in a separate nested element as400:config-connection
* Command Call - the command is passed in the nested tag as400:cmd
* Write Data Queue - the message entry is passed in the nested tag as400:dq-entry
* Read Data Queue - the entry key is placed into attributes.key 
* Program Call - the parameter value attribute was changed from parmValue-ref to parmValue; for data structure element definitions, the tag as400:data-structure-element is renamed to as400:parameter. 

=== Features

This release ports all previously implemented connector features to be compatible with Mule 4

=== Fixed in this Release
None

=== Known Issues
None


== Version 1.0.2 - October 2017

=== Version Compatibility

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 3.6.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Migrating from Older Versions
The new version of connector is fully backward compatible with older versions.

=== Features

* Program Call operation / processor that has no limit on the number of parameters,
supports most RPG data types (including date / time / timestamp), as well as data structures and arrays
* Secure TLS communications with IBM i (AS400) is now supported

=== Fixed in this Release
* Blank library list in Connection definition caused connection issues for IBM i OS versions 7.2 and 7.3

=== Known Issues
* Due to Anypoint Studio behavior the parameter definitions added via connector GUI may be out of
sequence after the Mule application changes are saved. Developers must go to XML view of the Mule code and
confirm / update the parameter sequence to match IBM i program parameters.

== Version 1.0.0 - November 2015

=== Compatibility Matrix

[width="100%", options="header"]
|=======
|Application/Service |Version
|Mule Runtime	| 3.5.0 or higher
|IBM i / OS400  | V5R4 or higher
|=======

=== Supported Operations:
[cols=",",options="header"]
|===
|Operation |Description
|Read Data Queue (Message Source) | Perpetually listen for new messages arriving to specific data queue
|Read Data Queue (Processor) | Read messages from specific data queue as part of Mule flow
|Write to Data Queue | Write messages to data queue
|Command Call | Execute AS400 command call
|===


=== Features and Functionality

* Works with keyed and non-keyed data queues
* Enables "peek" functionality to read message without removing it from the queue
* Automatically recovers stale connections

=== Fixed in this Release
None.

=== Known Issues
None.

== Contact Us

link:http://www.infoviewsystems.com/contact-us[Contact us] for connector pricing info, trial license, or support questions. 


== See Also

* More IBM i technical articles, source code samples and how-to's refer to  link:http://www.infoviewsystems.com/blog[Infoview Blog]
* link:https://blogs.mulesoft.com/dev/connectivity-dev/how-to-build-an-ibm-i-as400-api-in-15-minutes/[Build IBM i API in 15 minutes]
*  link:https://videos.mulesoft.com/watch/SXNPhnXutmy6tS4vqYUo9Y[Mulesoft Legacy Modernization webinar featuring IBM i connector]
